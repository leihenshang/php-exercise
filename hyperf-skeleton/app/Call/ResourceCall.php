<?php
declare(strict_types=1);

namespace App\Call;

use GuzzleHttp\Exception\GuzzleException;

class ResourceCall extends BaseCall
{

    public static function getUrl()
    {
        return 'http://develop.kingchannels.cn:50109';
    }

    /**
     * 通过SkuId获取资源信息
     * @param int $skuId
     * @param bool $withExtend 是否需要扩展字段
     * @return array
     */
    public static function getResourceInfoBySkuId(int $skuId,bool $withExtend = false): array
    {
        $resData = ['请求异常', null];
        $client = new \GuzzleHttp\Client();
        try {
            $getParams = $withExtend ? ['Id' => $skuId, 'withExtend' => $withExtend] : ['Id' => $skuId];
            $res = $client->get(self::getUrl() . '/resource/php/sku/detail', ['query' => $getParams]);
        } catch (GuzzleException $e) {
            return $resData;
        }

        $res = self::checkData($res);
        if (is_array($res)) {
            return [null, $res];
        }

        return ['返回数据异常', null];
    }


}