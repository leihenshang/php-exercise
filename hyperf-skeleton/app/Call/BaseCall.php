<?php
namespace App\Call;

abstract class BaseCall
{
   abstract static public function getUrl();

    /**
     * 检查数据
     * @param $res
     * @return mixed|null
     */
   public static function checkData($res)
   {
       if ($res->getStatusCode() !== 200) {
           return null;
       }

       $body = json_decode((string)$res->getBody(), true);
       if (empty($body) || !isset($body['Data'])) {
           return null;
       }

       $data = $body['Data'] ?? null;
       if(!$data) {
           return null;
       }

       return $data;
   }
}