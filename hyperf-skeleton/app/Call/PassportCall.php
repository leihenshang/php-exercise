<?php
declare(strict_types=1);

namespace App\Call;

use GuzzleHttp\Exception\GuzzleException;

class PassportCall extends BaseCall
{

    public static function getUrl()
    {
        return 'http://develop.kingchannels.cn:50107';
    }

    /**
     * 获取指定资源所属店铺
     * @param array $idArr
     * @param array $queryParams
     * @return array
     */
    public static function getContentsFromOrg(array $idArr,array $queryParams): array
    {
        $resData = ['请求异常', null];
        $client = new \GuzzleHttp\Client();
        $queryParam = array_merge(['ids' => implode(',', $idArr)],$queryParams);
        try {
            $res = $client->get(self::getUrl() . '/passport/net/userobject/getorganizationsforcontents', [
                'query' => $queryParam
            ]);
        } catch (GuzzleException $e) {
            return $resData;
        }

        $res = self::checkData($res);
        if (is_array($res)) {
            return [null, $res];
        }

        return ['返回数据异常', null];
    }

}