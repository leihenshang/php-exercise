<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Controller;

use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;
use Hyperf\Validation\Contract\ValidatorFactoryInterface;
use Psr\Container\ContainerInterface;

abstract class BaseAbstractController
{
    /**
     * 临时用户id
     * @var int
     */
    protected $tmpUserId = 1;

    /**
     * @Inject
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @Inject
     * @var RequestInterface
     */
    protected $request;

    /**
     * @Inject
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @Inject
     * @var ValidatorFactoryInterface
     */
    protected $validationFactory;

    /**
     * 成功的返回
     * @param $data
     * @param string $msg
     * @param int $code
     * @return array
     */
    public static function returnSuccess($data = null,string $msg = 'success',int $code = 200)
    {
        return [
            'Data' => $data,
            'Msg' => $msg,
            'Code' => $code
        ];
    }

    /**
     * 失败的返回
     * @param int $code
     * @param string $msg
     * @param null $data
     * @return array
     */
    public static function returnFailed(string $msg= 'failed',int $code = 14,$data = null)
    {
       return static::returnSuccess($data,$msg,$code);
    }
}
