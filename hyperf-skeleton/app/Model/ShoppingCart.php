<?php

declare (strict_types=1);
namespace App\Model;

use Hyperf\DbConnection\Model\Model;
/**
 * @property int $Id 
 * @property int $Count 
 * @property string $Price 
 * @property string $TotalMoney 
 * @property string $CreateTime 
 * @property int $ObjectId 
 * @property int $ObjectTypeId 
 * @property int $SkuId 
 * @property int $AppId 
 * @property int $SaleStrategyId 
 * @property int $IsSilentlyJoin 
 * @property string $ExternalId 
 * @property int $IsDeleted 
 * @property int $PurchaserId 
 * @property string $PurchaserName 
 * @property string $CustomData 
 * @property int $PurchaserType 
 * @property int $ObjectTypeTitle 
 * @property int $GoodsType 
 * @property int $Score 
 * @property int $ShopId 
 * @property int $CouponId 
 * @property string $Freight 
 */
class ShoppingCart extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ShoppingCart';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['Id' => 'integer', 'Count' => 'integer', 'ObjectId' => 'integer', 'ObjectTypeId' => 'integer', 'SkuId' => 'integer', 'AppId' => 'integer', 'SaleStrategyId' => 'integer', 'IsSilentlyJoin' => 'integer', 'IsDeleted' => 'integer', 'PurchaserId' => 'integer', 'PurchaserType' => 'integer', 'ObjectTypeTitle' => 'integer', 'GoodsType' => 'integer', 'Score' => 'integer', 'ShopId' => 'integer', 'CouponId' => 'integer'];


    public $timestamps = false;

    protected $primaryKey = 'Id';

}