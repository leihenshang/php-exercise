<?php


namespace App\Utils;

use Hyperf\Snowflake\Configuration;
use Hyperf\Snowflake\IdGenerator\SnowflakeIdGenerator;
use Hyperf\Snowflake\MetaGenerator\RandomMilliSecondMetaGenerator;
use Hyperf\Snowflake\MetaGeneratorInterface;

class SnowflakeUtils
{
    /**
     * @param int $num
     * @return array
     */
    public static function getIds(int $num = 1)
    {
        $config = new Configuration();
        $generator = new SnowflakeIdGenerator(new RandomMilliSecondMetaGenerator($config, MetaGeneratorInterface::DEFAULT_BEGIN_SECOND));
        if($num === 1) {
            $id = $generator->generate();
            return ['Id' => $id,'ExternalId' => 'external-'.$id];
        }

        $idArr = [];
        for ($i = 0; $i < $num; $i ++) {
            $tmp = (clone $generator)->generate();
            $idArr[] = ['Id' => $tmp,'ExternalId' => 'external-'.$tmp];
        }

        return $idArr;
    }

}