<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace HyperfTest\Cases;

use App\Jwt\SelfJwt;
use App\Middleware\JwtParse;
use App\Middleware\SelfJwtAuth;
use HyperfTest\HttpTestCase;

/**
 * @internal
 * @coversNothing
 */
class ShoppingCartTest extends HttpTestCase
{
    public function testDetailShouldReturnArray()
    {
        $res = $this->get('/ecommerce/php/shoppingcart/detail',[
        JwtParse::X_PUBLIC_ACCESS_TOKEN_KEY => SelfJwt::encode(["user" => json_encode(['UserId' => 1, 'IsAdmin' => 1])])
        ]);

        $this->assertTrue(is_array($res),'没有返回一个数组');
        $this->assertArrayHasKey('Data', $res, '结果中没有返回Data');
        $this->assertArrayHasKey('Code', $res, '结果中没有返回Code');
        $this->assertArrayHasKey('Msg', $res, '结果中没有返回Msg');
    }

    public function testCreateShouldReturnArray()
    {
        $res = $this->post('/ecommerce/php/shoppingcart/create');
        $this->assertTrue(is_array($res),'没有返回一个数组');
        $this->assertArrayHasKey('Data', $res, '结果中没有返回Data');
        $this->assertArrayHasKey('Code', $res, '结果中没有返回Code');
        $this->assertArrayHasKey('Msg', $res, '结果中没有返回Msg');
    }


    public function testUpdateShouldReturnArray()
    {
        $res = $this->post('/ecommerce/php/shoppingcart/update');
        $this->assertTrue(is_array($res),'没有返回一个数组');
        $this->assertArrayHasKey('Data', $res, '结果中没有返回Data');
        $this->assertArrayHasKey('Code', $res, '结果中没有返回Code');
        $this->assertArrayHasKey('Msg', $res, '结果中没有返回Msg');
    }

    public function testDeleteShouldReturnArray()
    {
        $res = $this->post('/ecommerce/php/shoppingcart/delete');
        $this->assertTrue(is_array($res),'没有返回一个数组');
        $this->assertArrayHasKey('Data', $res, '结果中没有返回Data');
        $this->assertArrayHasKey('Code', $res, '结果中没有返回Code');
        $this->assertArrayHasKey('Msg', $res, '结果中没有返回Msg');
    }


    public function testListShouldReturnArray()
    {
        $res = $this->get('/ecommerce/php/shoppingcart/list');
        $this->assertTrue(is_array($res),'没有返回一个数组');
        $this->assertArrayHasKey('Data', $res, '结果中没有返回Data');
        $this->assertArrayHasKey('Code', $res, '结果中没有返回Code');
        $this->assertArrayHasKey('Msg', $res, '结果中没有返回Msg');
    }
}
