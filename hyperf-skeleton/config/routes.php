<?php

declare(strict_types=1);

/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

use Hyperf\HttpServer\Router\Router;

Router::addRoute(['GET', 'POST', 'HEAD'], '/', 'App\Controller\IndexController@index');

//Router::addGroup('/ecommerce/php/shoppingCart/', function () {
//    Router::post('create', 'App\Controller\ShoppingCartController@create');
//    Router::post('delete', 'App\Controller\ShoppingCartController@delete');
//    Router::post('detail', 'App\Controller\ShoppingCartController@detail');
//    Router::post('listByIds', 'App\Controller\ShoppingCartController@listByIds');
//    Router::post('list', 'App\Controller\ShoppingCartController@list');
//    Router::post('update', 'App\Controller\ShoppingCartController@update');
//    Router::post('total', 'App\Controller\ShoppingCartController@total');
//    Router::post('calculatePrice', 'App\Controller\ShoppingCartController@calculatePrice');
//});

Router::get('/favicon.ico', function () {
    return '';
});

