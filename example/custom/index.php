<?php

require_once './vendor/autoload.php';

use Tangzq\Custom\User\Client;
use Tangzq\Custom\Order\Order;

$client = new Client();
$client->show();

$order = new Order();
$order->show();