# 如何自定义一个composer包

步骤大致如下

## 第一步

使用composer init，录入初始化信息，生成composer.json

```php
{
    "name": "tangq/custom",
    "description": "test",
    "type": "project",
    "license": "mit",
    "authors": [
        {
            "name": "tangzq",
            "email": "tangzq@Kingchannels.com"
        }
    ],
    "require": {
        "php" : ">=7.2"
    }
}

```

## 第二步

- 创建目录 mkdir src
- 创建目录 mkdir src/User

## 第三步

加入autoload psr-4字段，映射命名空间和实际的文件目录。注意以下composer.json中的 autoload 字段

```php
{
    "name": "tangq/custom",
    "description": "test",
    "type": "project",
    "license": "mit",
    "authors": [
        {
            "name": "tangzq",
            "email": "tangzq@Kingchannels.com"
        }
    ],
    "require": {
        "php" : ">=7.2"
    },
    "autoload": {
        "psr-4": {
            "Tangzq\\Custom\\":"src"
        }
    }
}
```

## 第四步

在src/User添加文件 Client.php

```php
<?php
namespace Tangzq\Custom\User;

class Client
{

public $name = "tangzq";
public $age = "50";

public function show()
{
    echo "{$this->name} - {$this->age}";
}
}
```

注意以上的命名空间 `namespace Tangzq\Custom\User;` 部分，由于映射了 `"Tangzq\\Custom\\":"src"` 所以我们的命令空间声明要使用映射的那个`"Tangzq\Custom\` 开头，后面接所在文件夹,比如这里的Cient就是在 src/User目录下。

## 第五步

使用composer install 生成vendor文件夹，以及自动加载文件。执行命令后会生成`vendor/autoload.php` 文件。

## 最后

进行测试，创建index.php文件，写入如下代码：

```php
<?php
//引入自动加载文件
require_once './vendor/autoload.php';

use Tangzq\Custom\User\Client;
use Tangzq\Custom\Order\Order;

$client = new Client();
$client->show();

$order = new Order();
$order->show();

//输出
//tangzq - 50

```

成功输出结果就成功了。

## 备注

有时候提示找不类文件，可能是你修改了代码目录的文件夹名字或大小写，可以使用 composer dumpautoload 命令刷新文件映射缓存。
