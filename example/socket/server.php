<?php
$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

if (socket_bind($socket, "127.0.0.1", 30000) === false) {
    printf('server bind failed:' . socket_strerror(socket_last_error()));
}

if (socket_listen($socket, 4) == false) {
    printf('server listen failed:' . socket_strerror(socket_last_error()));
}

do {
    $acceptResource = socket_accept($socket);
    if ($acceptResource !== false) {
        $str = socket_read($acceptResource, 1024);
    }

    echo 'server receive is :' . $str . PHP_EOL;
    if ($str != false) {
        $returnClient = 'server receive is:' . $str . PHP_EOL;
        socket_write($acceptResource, $returnClient, strlen($returnClient));
    } else {
        echo 'socket_read is failed' . PHP_EOL;
    }
} while (true);

socket_close($socket);
