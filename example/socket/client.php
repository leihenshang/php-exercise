<?php

$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

// socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, ['sec' => 1, 'usec' => 0]);
// socket_set_option($socket, SOL_SOCKET, SO_SNDTIMEO, ['sec' => 6, 'usec' => 0]);

if (socket_connect($socket, '127.0.0.1', 30000) == false) {
    echo '连接失败：' . socket_strerror(socket_last_error());
} else {
    $msg = 'you are good!皮皮猪';
    // $msg = mb_convert_encoding($msg, 'GBK', "UTF-8");

    if (socket_write($socket, $msg, strlen($msg)) == false) {
        echo "写入失败:" . socket_strerror(socket_last_error());
    } else {
        echo "客户端写入成功" . PHP_EOL;
        while ($callback = socket_read($socket, 1024)) {
            echo "服务端返回消息:" . PHP_EOL . $callback;
        }
    }
}

socket_close($socket);
